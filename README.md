# rms-support-letter

This repository is made for those who wish to support Richard Stallman, yet (for ethical reasons) wish to stay away from github.

There is a script that automatically syncs this to the main support letter right here: https://github.com/rms-support-letter/rms-support-letter.github.io

Please go to issue #1 and follow the instructions.

Note: If your message later on shows as NULL, that means you have to verify via your link to prove that the link is yours. If it's a website, I'll contact the owner of the website to check if the owner actually signed the letter.

To keep it simple for the syncing, please don't make new issues to sign the support letter, just do it via the first issue.

If you happen to visit this page but don't have/want to make a codeberg account, you can send a signed patch to signrms@prog.cf to show your support :)

If you have any questions, please stop by one of these rooms:

Matrix.org: #free-rms:matrix.org
IRC: #free-rms at chat.freenode.net
XMPP: Coming Soon?

Thank you :)